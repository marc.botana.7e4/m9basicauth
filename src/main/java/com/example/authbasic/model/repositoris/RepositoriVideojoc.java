package com.example.authbasic.model.repositoris;

import com.example.authbasic.model.entitats.Videojoc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositoriVideojoc extends JpaRepository<Videojoc, Long> {

}
